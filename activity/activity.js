let firstNameLabel = document.querySelector("#label-first-name");
let lastNameLabel = document.querySelector("#label-last-name");


let inputFirstName = document.querySelector("#txt-first-name");

console.log(inputFirstName.value);

inputFirstName.addEventListener('keyup',()=>{
	console.log(inputFirstName.value);
})


let fullNameDisplay = document.querySelector("#full-name-display");
let inputLastName = document.querySelector("#txt-last-name");


inputLastName.addEventListener('keyup',()=>{

	console.log(inputLastName.value);

})

//Syntax of addEventListener:
//element.addEventListener(<event>,<function>);

//You can create/add named functions as the function for the addEventListener.

const showName = () => {

	console.log(inputFirstName.value);
	console.log(inputLastName.value);
	//console.log(fullNameDisplay.innerHTML);
	fullNameDisplay.innerHTML = inputFirstName.value+" "+inputLastName.value;
}

inputFirstName.addEventListener('keyup',showName);
inputLastName.addEventListener('keyup',showName);


let submitButton = document.querySelector("#submit-btn");

submitButton.addEventListener('click',()=>{
if (inputFirstName.value.length !== 0 && inputLastName.value.length !== 0) {
	alert(`Thank you for registering, ${inputFirstName.value} ${inputLastName.value}  !`)
	window.location.assign("https://www.zuitt.co")
} else {
	alert("Please input first name and last name.")
}
	
})






