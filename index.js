//console.log("Hello World!");

/*
	JS DOM - Javascript Document Object Model

	For CSS, All html elements were considered as a box.
	This is our CSS Box Model. For Javascript, all HTML
	elements is considered as an object. This is what we
	call JS Document Object Model

	Since in JS, all html elements are objects and can be
	selected, we can manipulate and interactions to our web
	pages using javascript.
*/

/* document is a keyword in JS which refers to the whole
document. To the whole html page.
*/
console.log(document);

//store/select a particular element from our page to a variable

let firstNameLabel = document.querySelector("#label-first-name");

console.log(firstNameLabel);

let lastNameLabel = document.querySelector("#label-last-name");

console.log(lastNameLabel);
console.log(firstNameLabel.innerHTML);

firstNameLabel.innerHTML = "I like New York City.";
lastNameLabel.innerHTML = "My favorite food is seafood.";

let city = "Tokyo";

if(city === "New York"){
	firstNameLabel.innerHTML = `I like New York City.`
} else {
	firstNameLabel.innerHTML = `I don't like New York. I like ${city} city.`
}

//Events

firstNameLabel.addEventListener('click',()=>{
	firstNameLabel.innerHTML = "I've been clicked. Send help!"

	firstNameLabel.style.color = "red";
	firstNameLabel.style.fontSize = "10vh";
})

lastNameLabel.addEventListener('click',()=>{
	lastNameLabel.innerHTML = "I've been clicked. Send help!"
	lastNameLabel.style.color = "pink";
	lastNameLabel.style.fontSize = "5vh";
	})

//keyup
let inputFirstName = document.querySelector("#txt-first-name");

console.log(inputFirstName.value);

inputFirstName.addEventListener('keyup',()=>{
	console.log(inputFirstName.value);
})



lastNameLabel.addEventListener('click',()=>{

/*	lastNameLabel.style.color = "violet";
	lastNameLabel.style.fontSize = "5vh";*/

	//Check the current color of the lastNameLabel
	if(lastNameLabel.style.color === "violet"){
		lastNameLabel.style.color = "black";
		lastNameLabel.style.fontSize = "16px";
	} else {
		lastNameLabel.style.color = "violet";
		lastNameLabel.style.fontSize = "5vh"
	}
})

//keyup - is an event wherein we able to perform a task when the use lets go of a key. Keyup is best used in input elements that require key inputs.

let fullNameDisplay = document.querySelector("#full-name-display");
let inputLastName = document.querySelector("#txt-last-name");

console.log(fullNameDisplay);
console.log(inputLastName);

//.value is a property of mostly input elements. It contains the current value of the element.

//Initially, our input elements are empty.
//So, initially, our .value property for our inputFirstName is blank.
//Why is the .value still blank when we type into the input?
//This console log only outputs and ran the first time our page was shown, therefore, any changes you typed into the input element will not be shown, because console.log() only ran once.
console.log(inputFirstName.value);

//To be able to log in the console, the current value of our input element as we type, we have to add an event.

//To log the current value of the inputFirstName every time we typed into the element.
/*inputFirstName.addEventListener('keyup',()=>{

	console.log(inputFirstName.value);

})*/

/*
	Mini-Activity

	Add an event listener to our inputLastName which will be able to log the current value of the inputLastName in the console whenever the user types in the input.
*/
inputLastName.addEventListener('keyup',()=>{

	console.log(inputLastName.value);

})

//Syntax of addEventListener:
//element.addEventListener(<event>,<function>);

//You can create/add named functions as the function for the addEventListener.

const showName = () => {

	console.log(inputFirstName.value);
	console.log(inputLastName.value);
	//console.log(fullNameDisplay.innerHTML);
	fullNameDisplay.innerHTML = inputFirstName.value+" "+inputLastName.value;
}

inputFirstName.addEventListener('keyup',showName);
inputLastName.addEventListener('keyup',showName);

